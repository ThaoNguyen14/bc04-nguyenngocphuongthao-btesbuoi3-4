function renderUnCompletedTodo() {
  let str = "";
  todoList
    .filter((todo) => !todo.completed)
    .map((todo) => {
      str += `              <li>
        <span> ${todo.text} </span>
        <div class="buttons">
          <button class="remove" onclick="deleteTodo(${todo.id}); renderUnCompletedTodo();saveTodoList();">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink"
              enable-background="new 0 0 40 40"
              id="Слой_1"
              version="1.1"
              viewBox="0 0 40 40"
              xml:space="preserve"
            >
              <g>
                <path
                  d="M28,40H11.8c-3.3,0-5.9-2.7-5.9-5.9V16c0-0.6,0.4-1,1-1s1,0.4,1,1v18.1c0,2.2,1.8,3.9,3.9,3.9H28c2.2,0,3.9-1.8,3.9-3.9V16   c0-0.6,0.4-1,1-1s1,0.4,1,1v18.1C33.9,37.3,31.2,40,28,40z"
                />
              </g>
              <g>
                <path
                  d="M33.3,4.9h-7.6C25.2,2.1,22.8,0,19.9,0s-5.3,2.1-5.8,4.9H6.5c-2.3,0-4.1,1.8-4.1,4.1S4.2,13,6.5,13h26.9   c2.3,0,4.1-1.8,4.1-4.1S35.6,4.9,33.3,4.9z M19.9,2c1.8,0,3.3,1.2,3.7,2.9h-7.5C16.6,3.2,18.1,2,19.9,2z M33.3,11H6.5   c-1.1,0-2.1-0.9-2.1-2.1c0-1.1,0.9-2.1,2.1-2.1h26.9c1.1,0,2.1,0.9,2.1,2.1C35.4,10.1,34.5,11,33.3,11z"
                />
              </g>
              <g>
                <path
                  d="M12.9,35.1c-0.6,0-1-0.4-1-1V17.4c0-0.6,0.4-1,1-1s1,0.4,1,1v16.7C13.9,34.6,13.4,35.1,12.9,35.1z"
                />
              </g>
              <g>
                <path
                  d="M26.9,35.1c-0.6,0-1-0.4-1-1V17.4c0-0.6,0.4-1,1-1s1,0.4,1,1v16.7C27.9,34.6,27.4,35.1,26.9,35.1z"
                />
              </g>
              <g>
                <path
                  d="M19.9,35.1c-0.6,0-1-0.4-1-1V17.4c0-0.6,0.4-1,1-1s1,0.4,1,1v16.7C20.9,34.6,20.4,35.1,19.9,35.1z"
                />
              </g>
            </svg>
          </button>
          <button class="complete" onclick="completeTodo(${todo.id}); renderUnCompletedTodo(); renderCompletedTodo(); saveTodoList();">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              data-name="Livello 1"
              id="Livello_1"
              viewBox="0 0 128 128"
            >
              <title />
              <path
                d="M64,0a64,64,0,1,0,64,64A64.07,64.07,0,0,0,64,0Zm0,122a58,58,0,1,1,58-58A58.07,58.07,0,0,1,64,122Z"
              />
              <path
                d="M87.9,42.36,50.42,79.22,40.17,68.43a3,3,0,0,0-4.35,4.13l12.35,13a3,3,0,0,0,2.12.93h.05a3,3,0,0,0,2.1-.86l39.65-39a3,3,0,1,0-4.21-4.28Z"
              />
            </svg>
          </button>
        </div>
      </li>`;
    });
  document.getElementById("todo").innerHTML = str;
}
function renderCompletedTodo() {
  let str = "";
  todoList
    .filter((todo) => todo.completed)
    .map((todo, index) => {
      str += `              <li>
        <span> ${todo.text} </span>
        <div class="buttons">
          <button class="remove" onclick="deleteTodo(${todo.id}); renderUnCompletedTodo();renderCompletedTodo();saveTodoList();">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink"
              enable-background="new 0 0 40 40"
              id="Слой_1"
              version="1.1"
              viewBox="0 0 40 40"
              xml:space="preserve"
            >
              <g>
                <path
                  d="M28,40H11.8c-3.3,0-5.9-2.7-5.9-5.9V16c0-0.6,0.4-1,1-1s1,0.4,1,1v18.1c0,2.2,1.8,3.9,3.9,3.9H28c2.2,0,3.9-1.8,3.9-3.9V16   c0-0.6,0.4-1,1-1s1,0.4,1,1v18.1C33.9,37.3,31.2,40,28,40z"
                />
              </g>
              <g>
                <path
                  d="M33.3,4.9h-7.6C25.2,2.1,22.8,0,19.9,0s-5.3,2.1-5.8,4.9H6.5c-2.3,0-4.1,1.8-4.1,4.1S4.2,13,6.5,13h26.9   c2.3,0,4.1-1.8,4.1-4.1S35.6,4.9,33.3,4.9z M19.9,2c1.8,0,3.3,1.2,3.7,2.9h-7.5C16.6,3.2,18.1,2,19.9,2z M33.3,11H6.5   c-1.1,0-2.1-0.9-2.1-2.1c0-1.1,0.9-2.1,2.1-2.1h26.9c1.1,0,2.1,0.9,2.1,2.1C35.4,10.1,34.5,11,33.3,11z"
                />
              </g>
              <g>
                <path
                  d="M12.9,35.1c-0.6,0-1-0.4-1-1V17.4c0-0.6,0.4-1,1-1s1,0.4,1,1v16.7C13.9,34.6,13.4,35.1,12.9,35.1z"
                />
              </g>
              <g>
                <path
                  d="M26.9,35.1c-0.6,0-1-0.4-1-1V17.4c0-0.6,0.4-1,1-1s1,0.4,1,1v16.7C27.9,34.6,27.4,35.1,26.9,35.1z"
                />
              </g>
              <g>
                <path
                  d="M19.9,35.1c-0.6,0-1-0.4-1-1V17.4c0-0.6,0.4-1,1-1s1,0.4,1,1v16.7C20.9,34.6,20.4,35.1,19.9,35.1z"
                />
              </g>
            </svg>
          </button>
          <button class="complete">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
          <path style="fill:#6AC259;" d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333  c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622  l140.894-140.898l31.309,31.309L174.199,322.918z"/>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          <g>
          </g>
          </svg>
          </button>
        </div>
      </li>`;
    });
  document.getElementById("completed").innerHTML = str;
}

function getTodoList() {
  return JSON.parse(window.localStorage.getItem("todoList"));
}

function saveTodoList() {
  window.localStorage.setItem("todoList", JSON.stringify(todoList));
}

function addTodo() {
  if (todoList) {
    todoList.push(new ToDo(document.getElementById("newTask").value));
  }
}

function deleteTodo(id) {
  if (todoList) {
    const foundIndex = todoList.findIndex((todo) => todo.id == id);
    todoList.splice(foundIndex, 1);
  }
}

function completeTodo(id) {
  if (todoList) {
    const foundIndex = todoList.findIndex((todo) => todo.id == id);
    todoList[foundIndex].completed = true;
  }
}
