var todoList = getTodoList() || [];

renderUnCompletedTodo();
renderCompletedTodo();

function validate() {
  return !!document.getElementById("newTask").value;
}

function onClickAddBtnHandler() {
  if (!validate()) {
    document.getElementById("error-text").style.display = "block";
    return;
  }
  addTodo();
  renderUnCompletedTodo();
  saveTodoList();
  renderCompletedTodo();
}
